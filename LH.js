import root from 'window-or-global';

const LH = root.LH ? root.LH : [];

LH.idMap = {
	"sortByPrice": "A",
	"largeButtons": "B",
	"showRecentlyViewed": "F"
};

LH.isEnabled = feature => {
	return LH.some(item => {
		return item === feature;
	});
};

LH.getEnabledIds = () => {
	return LH.reduce((result, next) => {
		return result + LH.idMap[next];
	}, "");
};

export default LH;