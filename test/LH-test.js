import {assert} from 'chai';
import LH from '../LH';

describe('LH', () => {
	it('isEnabled method returns correct boolean if feature is enabled', () => {
		LH.push("sortByPrice");

		assert.isTrue(LH.isEnabled("sortByPrice"));
		assert.isFalse(LH.isEnabled("largeButtons"));
	});

	it("getEnabledIds method returns correct string representation of available features", () => {
		LH.push("showRecentlyViewed");

		assert.equal(LH.getEnabledIds(), "AF");
	});
});